# libinput-mouse-tweaks

A Bash script to tweak mouse sensitivity and acceleration

## Instructions

1. `chmod +x run.sh`
2. `./run.sh`

## Keybindings

- Up/Down arrow to increase/decrease 0.10x.
- Left/Right arrow to switch between Acceleration and Sensitivity.
- Any other key finishes the wizard.

### Notes

- `Coordinate Transformation Matrix` is the sensitivity
	- Default is 1x
- `libinput Accel Speed` is the acceleration multiplier
	- No acceleration is 1x

## Permanently applying changes

1. Finish the wizard to get the code snippet, for example:

```perl
Section "InputClass"
	Identifier	"libinput pointer catchall"
	MatchIsPointer	""
	MatchProduct	"Logitech Gaming Mouse G402"
	MatchDevicePath	"/dev/input/event*"
	Driver "libinput"
	Option "CalibrationMatrix" "1.000000, 0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 1.000000"
	Option "AccelSpeed" "0.000000"
EndSection
```

1. Paste the code in `/etc/X11/xorg.conf.d/99-mouse.conf` (or `xorg.conf`, although not recommended)

2. Reboot

## Screenshot

![Screenshot of the script in action](http://u.cubeupload.com/diamondburned/slbStI.png)

## Credits

- Dylan for the [Bible](https://github.com/dylanaraps/pure-bash-bible/blob/master/README.md)
- Some guys on Unixporn
- ~~ging_osu~~