#!/bin/bash

trim_string() {
    read -r input
    : "${input#"${input%%[![:space:]]*}"}"
    : "${_%"${_##*[![:space:]]}"}"
    printf '%s\n' "$_"
}

nofirstline() {
    IFS=$'\n' read -d "" -ra arr
    printf '%s\n' "${arr[@]:1}"
}

udlr() {
    escape_char=$(printf "\u1b")
    read -rsn1 mode # get 1 character
    if [[ $mode == $escape_char ]]; then
        read -rsn2 mode # read 2 more chars
    fi
    case $mode in
        '[A') echo -n '1' ;; # Up
        '[B') echo -n '2' ;; # Down
        '[D') echo -n '3' ;; # Left
        '[C') echo -n '4' ;; # Right
        *)    echo -n '0' ;; # Exit
    esac
}

chgCalc() {
    case $1 in
        1)  echo "$2 + .1" | bc -l;;
        2)  echo "$2 - .1" | bc -l;;
    esac
}
