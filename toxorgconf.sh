#!/bin/bash
function toxorgconf() {
	NAME="$1"
	MATRIX="$2"
	ACCEL="$3"

	file=$(cat<<EOF
Section "InputClass"
	Identifier	"libinput pointer catchall"
	MatchIsPointer	""
	MatchProduct	"$NAME"
	MatchDevicePath	"/dev/input/event*"
	Driver	"libinput"
	Option "CalibrationMatrix" "$MATRIX"
	Option "AccelSpeed" "$ACCEL"
EndSection
EOF
)

	echo "$file"
}