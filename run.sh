#!/bin/bash
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
##

source thanks-dylan.sh
source toxorgconf.sh

pointers=$(xinput | grep -F '⎜   ↳' )

IFS=$'\n'

arr=("libinput Accel Speed" "Coordinate Transformation Matrix")
cycle() {
    printf '%s' "${arr[${i:=0}]}"
    ((i=i>=${#arr[@]}-1?0:++i))
}

echo "Please pick one of the following devices:"

ids=()
for id in $(echo "$pointers" | cut -d= -f2); do
    ids+=( "$(trim_string <<< "${id%%[*}")" )
done

int=0
names=()
for name in $(echo "$pointers" | cut -d= -f1); do
    names+=( "$(trim_string <<< "${name:6:-2}")" )
    echo "$int: [${ids[$int]}] ${names[$int]}"
    ((int++))
done

read -rp "Device number: " CHOICE

getDevice() {
    id="$1"
    properties='libinput Accel Speed \(|Coordinate Transformation Matrix'
    props=$(xinput list-props "${id}" | grep -E "$properties") || {
        echo "Invalid choice!"
        exit 1
    }

    if [[ ! "$props" =~ "libinput" ]]; then
        echo "Not a libinput device! Exiting."
        exit 1
    fi

    keys=()
    for key in $(cut -d: -f1 <<< "$props"); do
        keys+=( "$(trim_string <<< "$key")" )
    done

    values=()
    for value in $(cut -d: -f2 <<< "$props"); do
        values+=( "$(trim_string <<< "$value")" )
    done

    printf "%s\n" "${keys[@]}"
    printf "%s\n" "${values[@]}"
}

str=$(getDevice "${ids[$CHOICE]}")
keys=(   $(echo "$str" | head -n2) ) # matrix 0 accel 1
values=( $(echo "$str" | tail -n2) )
currentProperty=$(cycle)
echo $'\n'"Now changing $currentProperty"
while true; do
    arrow=$(udlr)
    str=$(getDevice "${ids[$CHOICE]}")
    keys=(   $(echo "$str" | head -n2) ) # matrix 0 accel 1
    values=( $(echo "$str" | tail -n2) )
    case $arrow in 
        1|2)
            case "$currentProperty" in
                *"Accel"*)
                    cur="${values[1]}"
                    chg=$(chgCalc "$arrow" "$cur")
                    xinput set-prop "${ids[$CHOICE]}" "$currentProperty" "$chg" &> /dev/null && {
                        echo $'\t'"Acceleration: ${chg}x"
                    } || echo $'\t<!> '"$chg not in range from -1 to 1"
                    ;;
                *"Matrix"*)
                    IFS=", " read -d "" -ra matrixArr <<< "${values[0]}"
                    cur="${matrixArr[0]}"
                    chg=$(chgCalc "$arrow" "$cur")
                    xinput set-prop "${ids[$CHOICE]}" "$currentProperty" "$chg" 0 0 0 "$chg" 0 0 0 1 && {
                        echo $'\t'"Sensitivity: ${chg}x"
                    }
                    ;;
            esac
            ;;
        3|4)
            cycle > /dev/null # workaround
            currentProperty=$(cycle)
            echo $'\n'"Now changing $currentProperty"
            ;;
        *)
            echo $'\n'"Completed. To apply on startup, add this into your xorg.conf(.d):"
            toxorgconf "${names[$CHOICE]}" "${values[0]}" "${values[1]}"
            break
            ;;
    esac
done
